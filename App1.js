const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get('/clientes',(req,res)=>{
    res.send("{message:clientes encontrado}");
});

app.get('/clientes/app1',(req,res)=>{
    let source = req.query;
    let ret = "Nome:" + source.nome;
    res.send("{Mensagem: "+ret+"}");
});

app.post('/clientes',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["acess"]

    let ret = "Dados do cliente: Nome: " + dados.nome;
    ret+=" Bairro: " + dados.bairro;
    ret+=" Empresa: " + dados.empresa;
    
    if(headers_ == "2097"){
        res.send("{Mensagem: "+ret+"}");
    }else{
        res.send("Credencial Inválida! ")
    }
});



app.get('/funcionarios/app1',(req,res)=>{
    let source = req.query;
    let ret = "Nome:" + source.nome;
    res.send("{Mensagem: "+ret+"}");
});


app.delete('/funcionarios/delete/:valor',(req, res)=>{
    let dados = req.params.valor
    let headers_ = req.headers["acess"]

    if(headers_ == "2097"){
        res.send("{Mensagem: Valor: " + dados + "}");
    }else{
        res.send("Credencial Inválida!")
    }
});

app.put('/funcionarios',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["acess"]

    let ret = "Dados do Funcionario: Nome do Funcionario: " + dados.nomeF;
    ret+=" Filial: " + dados.Filial;
    
    if(headers_ == "2097"){
        res.send("{Mensagem: "+ret+"}");
    }else{
        res.send("Credencial Inválida! ")
    }
});