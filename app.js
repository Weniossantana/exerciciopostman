const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

//habilitar servidor
app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get('/aluno',(req,res)=>{
    res.send("{message:aluno encontrado}");
});

//recurso de request.query
app.get('/aluno/filtros',(req,res)=>{
    let source = req.query;
    let ret = "Dados enviados:" + source.nome + " " + source.sobrenome;
    res.send("{message:"+ret+"}");
});

//recurso de request.param
app.get('/aluno/pesquisa/:valor',(req, res)=>{
    console.log("Entrou");
    let dado = req.params.valor;
    let ret = "Dados solicitado:" + dado;
    res.send("{message:"+ret+"}");
});

//recurso de request.body
app.post('/aluno',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["acess", "querocafe"]

    let ret = "Dados enviados: Nome:" + dados.nome;
    ret+=" Sobrenome: " + dados.sobrenome;
    ret+=" Idade: " + dados.idade;
    ret+=" Acess: " + headers_[0]
    ret+=" Quero: " + headers_[1]

    res.send("{message:"+ret+"}");
});